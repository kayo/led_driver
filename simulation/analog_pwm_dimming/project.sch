EESchema Schematic File Version 2
LIBS:kicad-spice
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 9000 5650 0    60   ~ 0
-pspice\n* \n.SUBCKT PC817C A K E C\nD1 A N001 LED\nQ1 C 5 E 0 NPN1 1\nR1 5 E 10G\nC1 A K 20p\nV1 N001 K 0\nR2 N002 0 1\nC2 N002 0 200n\nE1 0 N002 TABLE {I(V1)} =\n+ (0, 0)\n+ (10m, 10m)\n+ (15m, 14m)\n+ (20m, 17m)\n+ (40m, 25m)\n+ (80m, 35m)\n+ (160m, 50m)\nG1 E 5 N002 0 0.035\nC3 C 5 10p\nC4 A C 0.3p\nC5 K E 0.3p\nC6 5 E 10p\n.MODEL LED D\n+ Is=1e-15\n+ Rs=4\n+ N=1.5\n+ Eg=1.7\n+ CJO=30p\n+ TT=100n\n.MODEL NPN1 NPN\n+ Is=1e-12\n+ BF=200\n+ VAF=80\n+ IKF=0.025\n+ ISE=1e-9\n+ NE=2\n+ CJC=20p\n+ CJE=50p\n+ TF=5e-8\n+ Rb=100\n+ Rc=20\n+ Re=5\n.ENDS
Text Notes 6600 6200 0    60   ~ 0
+pspice\n* \n.control\n  option nopage\n  tran 1uS 400mS 0mS\n  set gnuplot_terminal=png\n  gnuplot project\n*  + v(pwm)\n  + v(in)\n  + v(out)\n  + v(ref)\n*  + v(vdd)\n.endc
$Comp
L V V1
U 1 1 574F6992
P 4400 3700
F 0 "V1" V 4200 3700 60  0000 C CNN
F 1 "PULSE 0V 3.3V 0uS 1uS 1uS 85uS 170uS" H 4400 3050 60  0000 C CNN
F 2 "" H 4075 3700 60  0000 C CNN
F 3 "" H 4075 3700 60  0000 C CNN
	1    4400 3700
	1    0    0    -1  
$EndComp
$Comp
L OC XO1
U 1 1 574F6AA2
P 5400 3700
F 0 "XO1" H 5400 3900 60  0000 C CNN
F 1 "PC817C" H 5375 3500 60  0000 C CNN
F 2 "" H 5150 3700 60  0000 C CNN
F 3 "" H 5150 3700 60  0000 C CNN
	1    5400 3700
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 574F6BD3
P 4800 3300
F 0 "R1" H 4800 3400 50  0000 C CNN
F 1 "150" H 4800 3200 50  0000 C CNN
F 2 "" V 4730 3300 30  0000 C CNN
F 3 "" H 4800 3300 30  0000 C CNN
	1    4800 3300
	1    0    0    -1  
$EndComp
Text GLabel 4400 3300 0    60   Input ~ 0
PWM
$Comp
L 0 #GND01
U 1 1 574F6E0E
P 4400 4100
F 0 "#GND01" H 4400 4000 40  0001 C CNN
F 1 "0" H 4400 4030 40  0000 C CNN
F 2 "" H 4400 4100 60  0000 C CNN
F 3 "" H 4400 4100 60  0000 C CNN
	1    4400 4100
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND02
U 1 1 574F6F7B
P 5200 4100
F 0 "#GND02" H 5200 4000 40  0001 C CNN
F 1 "0" H 5200 4030 40  0000 C CNN
F 2 "" H 5200 4100 60  0000 C CNN
F 3 "" H 5200 4100 60  0000 C CNN
	1    5200 4100
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 574F71F9
P 6100 4300
F 0 "R2" H 6100 4400 50  0000 C CNN
F 1 "8.2K" H 6100 4200 50  0000 C CNN
F 2 "" V 6030 4300 30  0000 C CNN
F 3 "" H 6100 4300 30  0000 C CNN
	1    6100 4300
	0    -1   -1   0   
$EndComp
$Comp
L C C1
U 1 1 574F7409
P 5600 4300
F 0 "C1" H 5550 4450 50  0000 L CNN
F 1 "1u" H 5550 4150 50  0000 L CNN
F 2 "" H 5638 4150 30  0000 C CNN
F 3 "" H 5600 4300 60  0000 C CNN
	1    5600 4300
	0    -1   1    0   
$EndComp
$Comp
L V V2
U 1 1 574F75B7
P 7900 3700
F 0 "V2" V 7700 3700 60  0000 C CNN
F 1 "DC 7.5V" H 7900 3050 60  0000 C CNN
F 2 "" H 7900 3700 60  0000 C CNN
F 3 "" H 7900 3700 60  0000 C CNN
	1    7900 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3500 4400 3300
Wire Wire Line
	4400 3300 4600 3300
Wire Wire Line
	5000 3300 5200 3300
Wire Wire Line
	5200 3300 5200 3500
Wire Wire Line
	4400 3900 4400 4100
Wire Wire Line
	5200 3900 5200 4100
Wire Wire Line
	6800 2700 6800 2500
Wire Wire Line
	5600 3300 5600 3500
Wire Wire Line
	6800 2500 7900 2500
Wire Wire Line
	7900 2500 7900 3500
Wire Wire Line
	7900 3900 7900 4100
$Comp
L 0 #GND03
U 1 1 574F79A2
P 7900 4100
F 0 "#GND03" H 7900 4000 40  0001 C CNN
F 1 "0" H 7900 4030 40  0000 C CNN
F 2 "" H 7900 4100 60  0000 C CNN
F 3 "" H 7900 4100 60  0000 C CNN
	1    7900 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3900 5600 4200
Wire Wire Line
	5600 4400 5600 4700
$Comp
L 0 #GND04
U 1 1 574F7AA8
P 5600 4700
F 0 "#GND04" H 5600 4600 40  0001 C CNN
F 1 "0" H 5600 4630 40  0000 C CNN
F 2 "" H 5600 4700 60  0000 C CNN
F 3 "" H 5600 4700 60  0000 C CNN
	1    5600 4700
	1    0    0    -1  
$EndComp
Text GLabel 7900 3300 2    60   Input ~ 0
VDD
Text GLabel 5200 3300 1    60   Input ~ 0
IN
Text GLabel 5600 3300 1    60   Input ~ 0
OUT
Text Notes 2550 5600 0    60   ~ 0
/* pwm control */\n\nEQ[f[pwm]]: f[pwm] = f[in] / 2^w[bits];\nEQ[t[pwm]]: [t[pulse]=d[pwm]/f[pwm], t[period]=1/f[pwm]], EQ[f[pwm]];\n\nPR[in]: [f[in]=24e6, w[bits]=12, d[pwm]=50e-2];\n\nEQ[f[pwm]], PR[in];\nEQ[t[pwm]], PR[in];
Wire Wire Line
	6100 4500 6100 4700
$Comp
L 0 #GND05
U 1 1 574F88E9
P 6100 4700
F 0 "#GND05" H 6100 4600 40  0001 C CNN
F 1 "0" H 6100 4630 40  0000 C CNN
F 2 "" H 6100 4700 60  0000 C CNN
F 3 "" H 6100 4700 60  0000 C CNN
	1    6100 4700
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 574F88FD
P 6800 2900
F 0 "R3" H 6800 3000 50  0000 C CNN
F 1 "0R" H 6800 2800 50  0000 C CNN
F 2 "" V 6730 2900 30  0000 C CNN
F 3 "" H 6800 2900 30  0000 C CNN
	1    6800 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 4000 6100 4100
Text Notes 5300 2450 0    60   ~ 0
/* divider selection */\n\nEQ[R[out]]: solve(U[in] / (R[in] + R[out]) = U[out] / R[out], R[out])[1];\n\nPR[in]: [U[in]=7.5, U[out]=1.5, R[in]=5.6e3];\n\nEQ[R[out]], PR[in];
Text GLabel 6100 4000 2    60   Input ~ 0
REF
Wire Wire Line
	6100 4000 5600 4000
Connection ~ 5600 4000
Wire Wire Line
	5600 3300 6800 3300
Wire Wire Line
	6800 3300 6800 3100
$EndSCHEMATC
